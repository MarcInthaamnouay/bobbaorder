FROM rust:1.40-stretch

WORKDIR /usr/src/bobbaorder

COPY . .

RUN cargo build

# run the bin
CMD ["./target/debug/bobbaorder"]
