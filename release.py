#!/usr/bin/env python3
import git
import semver
import subprocess
import sys

def bump(version):
  ver = semver.parse_version_info(version)
  bump_type = get_arg()

  if bump_type in '#major':
    ver = ver.bump_major()
  elif bump_type in '#minor':
    ver = ver.bump_minor()
  else:
    ver = ver.bump_patch()

  print(ver)

def get_arg():
  arg = sys.argv
  return arg[1]

def main():
  repo = git.Repo("./")
  tags = sorted(repo.tags, key=lambda t: t.commit.committed_datetime)

  if not tags:
    version = "1.0.0"
    print(version)
    return 0
  else:
    version = tags[-2]

  bump(version)

main()