use std::{thread, time};
use std::borrow::BorrowMut;
use crate::core::logger::{log, Level};
use crate::core::cmd::{Kind};
use crate::queue::QUEUE;
use crate::actions::visit::Visitor;
use crate::err::{LOCK_ACQUIRE_DIFFERENT_THREAD_ERROR};
use crate::swear::loader::{load_static_dictionnary};
use crate::actions::{
    bobba,
    room,
    send,
    insult
};

// Constant
const CONSUME_INTERVAL: u64 = 50;

/// Bootstrap
///
/// # Description
/// Method use to retrieve any resources that will be consume during the entire life of the consumer
///
/// # Return
/// Option<String>
fn prepare_consumer() -> Option<String> {
    load_static_dictionnary()
}

/// Start Consumer
///
/// # Description
/// Start an infinite loop which will consume the Queue (FIFO)
///
/// # Example of how a FIFO work
/// |   | | B |   |       |
/// | A | | A | B | empty |
pub fn start_consumer() {
    let words = prepare_consumer();
    thread::spawn(move || loop {
        let lock = QUEUE.lock();
        if lock.is_err() {
            log(Level::Debug, LOCK_ACQUIRE_DIFFERENT_THREAD_ERROR);
            thread::sleep(time::Duration::from_millis(CONSUME_INTERVAL));
            continue;
        }

        let mut queue = lock.unwrap();
        let task = queue.borrow_mut().first();
        if let Some(t) = task {
            log(Level::Info, "Consumer: found a task");
            let kind = t.msg.clone().cmd.retrieve_command();
            match kind {
                Kind::Bobba => bobba::BobbaAction { packet: &t }.visit(),
                Kind::Room => room::RoomAction { packet: &t }.visit(),
                Kind::Insult => insult::InsultAction { packet: &t, words: &words }.visit(),
                Kind::Empty => send::SendAction { packet: &t }.visit()
            }

            // Remove the task from the queue
            queue.remove(0);
        }

        // Release the lock on the stack
        std::mem::drop(queue);
        // the lock should be released by now
        thread::sleep(time::Duration::from_millis(CONSUME_INTERVAL));
    });
}
