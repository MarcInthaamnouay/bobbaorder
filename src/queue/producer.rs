use std::{thread, time};
use ws::{Message, Sender};
use crate::packet::message::MessagePacket;
use crate::queue::{QUEUE, QueuePacket};
use crate::core::logger::{log, Level};
use crate::core::cmd::Command;
use crate::err::{LOCK_ACQUIRE_DIFFERENT_THREAD_ERROR};

// Errors constant
const EMPTY_PAYLOAD_ERROR: &str = "Producer: payload is empty";
const PARSE_PAYLOAD_ERROR: &str = "Unable to parse the message payload";

// Constant
const PRODUCER_INTERVAL: u64 = 100;

/// Push
///
/// # Description
/// Parse a Message and start a producer to push the Message into the
/// queue of message
///
/// # Arguments
/// * `msg` Message
/// * `s` Sender
pub fn push(msg: Message, s: Sender) {
    if msg.is_empty() {
        log(Level::Warning, EMPTY_PAYLOAD_ERROR);
        return;
    }

    let msg_content = match msg.as_text() {
        Ok(text) => text,
        Err(_) => {
            log(Level::Error, PARSE_PAYLOAD_ERROR);
            return;
        }
    };

    let msg_packet = match MessagePacket::new(msg_content) {
        Ok(packet) => packet,
        Err(err) => {
            log(Level::Error, err.as_str());
            return;
        }
    };

    run_producer(msg_packet, s);
}

/// Register Client
///
/// # Description
/// Register a client by pushing a new message into the Queue
///
/// # Arguments
/// * `s` Sender
pub fn register_client(s: Sender) {
    let packet = MessagePacket {
        id: "1".to_string(),
        sender_id: "1".to_string(),
        receiver_id: "default".to_string(),
        room_id: "1".to_string(),
        content: "".to_string(),
        cmd: Command {
            name: Some("room".to_string()),
            args: Some(vec!["join".to_string(), "default".to_string()])
        }
    };

    run_producer(packet, s);
}

/// Run Producer
///
/// # Description
/// Run a thread to insert a payload that need to be consume
///
/// # Arguments
/// * `packet` MessagePacket
/// * `s` Sender
fn run_producer(packet: MessagePacket, s: Sender) {
    thread::spawn(move || loop {
        let lock = QUEUE.lock();
        if lock.is_err() {
            log(Level::Warning, LOCK_ACQUIRE_DIFFERENT_THREAD_ERROR);
            thread::sleep(time::Duration::from_millis(PRODUCER_INTERVAL));
            continue;
        }

        let mut queue = lock.unwrap();
        log(Level::Info, "Producer: Add a task");
        queue.push(
            QueuePacket {
                s: s.clone(),
                msg: packet
            }
        );

        break;
    });
}
