use std::sync::{Mutex};
use ws::{Sender};
use crate::packet::message::{MessagePacket};

pub mod consumer;
pub mod producer;

/// Queue Packet
pub struct QueuePacket {
    pub s: Sender,
    pub msg: MessagePacket
}

lazy_static! {
    #[derive(Debug)]
    pub static ref QUEUE: Mutex<Vec<QueuePacket>> = Mutex::new(Vec::new());
}
