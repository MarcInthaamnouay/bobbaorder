use serde::{Deserialize, Serialize};

/// Command
///
/// # Description
/// struct which handle the representation of a command pass through the chat
///
/// # Example
/// /room create mamouse
#[derive(Deserialize, Serialize ,Clone)]
pub struct Command {
    pub name: Option<String>,
    pub args: Option<Vec<String>>
}

/// Kind
///
/// # Description
/// List of command type supported by the chat
pub enum Kind {
    Room,
    Bobba,
    Insult,
    Empty
}

/// Actions
///
/// # Description
/// Define a list of "possible" action that could be done by a command
pub enum Actions {
    Create,
    Join,
    List,
    Delete,
    Order,
    Empty
}

/// Trait to parse to get enum from string
pub trait EnumHelper<T, E> {
    /// From String
    ///
    /// # Description
    /// Create a <E> Datatype from an action
    ///
    /// # Arguments
    /// * `action` &str
    ///
    /// # Return
    /// E: any
    fn from_string(action: &str) -> E;
}

impl Command {
    /// Retrieve Command
    ///
    /// # Description
    /// Retrieve a command kind from the Command struct
    ///
    /// # Arguments
    /// * `self`
    ///
    /// # Return
    /// * `Kind`
    pub fn retrieve_command(self) -> Kind {
        if let Some(name) = self.name {
            return Kind::from_string(&name);
        }

        Kind::Empty
    }
}

impl EnumHelper<String, Kind> for Kind {
    fn from_string(kind: &str) -> Kind {
        match kind.to_lowercase().as_str() {
            "room" => Kind::Room,
            "bobba" => Kind::Bobba,
            "insult" => Kind::Insult,
            _ => Kind::Empty
        }
    }
}

impl EnumHelper<String, Actions> for Actions {
    fn from_string(action: &str) -> Actions {
        match action.to_lowercase().as_str() {
            "create" => Actions::Create,
            "join" => Actions::Join,
            "list" => Actions::List,
            "delete" => Actions::Delete,
            "order" => Actions::Order,
            _ => Actions::List
        }
    }
}

/// Get Action
///
/// # Description
/// Parse the arguments of a command
/// e.g: /room create <name>
///
/// # Arguments
/// * `args` String
///
/// # Return
/// (Actions, Vec<String>)
pub fn get_action(args: &Option<Vec<String>>) -> (Actions, Vec<String>) {
    if let Some(opts) = args {
        let action_str = match opts.first() {
            Some(a) => a,
            None => {
                return (Actions::Empty, Vec::new());
            }
        };

        let action = Actions::from_string(action_str);
        let mut opts_vec = opts.to_vec();
        opts_vec.remove(0);

        return (action, opts_vec);
    }

    (Actions::Empty, Vec::new())
}
