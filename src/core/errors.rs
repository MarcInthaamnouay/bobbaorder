pub mod room {
    pub const MISSING_NAME_ERROR: &str = "Unable to create room due to missing name parameter";
    pub const ROOM_EXIST_ERROR: &str = "Room already exist with name: ";
    pub const ROOM_NOT_EXIST_ERROR: &str = "Room does not exist";
    pub const USER_ALREADY_EXIST: &str = "User already exist";
    pub const NAME_EMPTY_ERROR: &str = "Name parameter is empty";
    pub const UNABLE_SEND_MESSAGE: &str = "Unable to send a message after 3 trials";
    pub const LOCK_ALREADY_ACQUIRE_ERROR: &str = "Room Lock is already acquire";
}
