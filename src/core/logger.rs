use colored::*;

/// Level
///
/// # Description
/// Define the level of severity of Log
pub enum Level {
    Debug,
    Info,
    Warning,
    Error
}

/// Log
///
/// # Arguments
/// * `l` Level
/// * `msg` &str
pub fn log(l: Level, msg: &str) {
    let content = match l {
        Level::Debug => format!("{} {}", "Debug:".green(), msg),
        Level::Info => format!("{} {}", "Info:".blue(), msg),
        Level::Warning => format!("{} {}", "Warning:".yellow(), msg),
        Level::Error => format!("{} {}", "Error".red(), msg)
    };

    println!("{}", content);
}
