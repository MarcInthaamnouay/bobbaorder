#[macro_use]
extern crate lazy_static;

mod socket;
mod core;
mod room;
mod queue;
mod packet;
mod err;
mod actions;
mod swear;
mod http;
mod tests;

fn main() {
    // starting the consumer
    match room::create_default_room() {
        Ok(_) => {
            queue::consumer::start_consumer();
            socket::bootstrap_socket();
        },
        Err(err) => panic!(err)
    }

    println!("foo bar !!");
}
