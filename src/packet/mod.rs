pub mod message;
pub mod response;
pub mod errors;
pub mod bobbas;

use ws::Message;
use serde::{Serialize};

/// Stringifier
/// 
/// # Description
/// Trait use to operate on the packet
pub trait Stringifier {
    /// To String
    /// 
    /// # Arguments
    /// * `&self`
    /// 
    /// # Return
    /// String
    fn to_string(&self) -> String where Self : Serialize {
        match serde_json::to_string(&self) {
            Ok(json) => json,
            Err(_) => String::new()
        }
    }
    /// To Message
    /// 
    /// # Arguments
    /// * `&self`
    /// 
    /// # Return
    /// ws::Message
    fn to_message(&self) -> Message where Self : Serialize {
        let content = &self.to_string();

        Message::from(content.to_owned())
    }
}
