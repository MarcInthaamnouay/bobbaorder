use serde::{Serialize};
use super::Stringifier;

/// Error Packet
/// 
/// # Description
/// Packet use for sending errors to the client
#[derive(Serialize)]
pub struct ErrorPacket {
    pub request: String,
    pub value: String,
}

impl Stringifier for ErrorPacket {}
