use std::error::Error;
use serde::{Deserialize, Serialize};
use serde_json;
use super::Stringifier;
use super::response::{ResponsePacket};
use crate::core::cmd::Command;

/// Message Packet
///
/// # Description
/// Packet use for incoming message
#[derive(Deserialize, Serialize, Clone)]
pub struct MessagePacket {
    pub id: String,
    pub sender_id: String,
    pub receiver_id: String,
    pub room_id: String,
    pub content: String,
    pub cmd: Command
}

impl MessagePacket {
    /// New
    ///
    /// # Description
    /// Create a new MessagePacket based on the inputed message
    ///
    /// # Arguments
    /// * `value` &str
    ///
    /// # Return
    /// Result<Self, String>
    pub fn new(value: &str) -> Result<Self, String> {
        match serde_json::from_str(value) {
            Ok(msg) => Ok(msg),
            Err(err) => Err(err.description().to_string())
        }
    }

    /// Create Message From Packet
    ///
    /// # Description
    /// Create a response packet
    ///
    /// # Arguments
    /// * `self` MessgePacket
    ///
    /// # Return
    /// ResponsePacket
    pub fn create_from_message_packet(self) -> ResponsePacket {
        ResponsePacket {
            room_id: self.room_id,
            content: self.content,
            sender_id: self.sender_id
        }
    }
}

impl Stringifier for MessagePacket {}
