use serde::{Serialize};
use serde_json;
use super::Stringifier;

/// Response Packet
/// 
/// # Description
/// Packet use for sending successfull response to the client
#[derive(Serialize)]
pub struct ResponsePacket {
    pub room_id: String,
    pub content: String,
    pub sender_id: String
}

impl ResponsePacket {
    pub fn new(room_id: String, content: String, s: String) -> ResponsePacket {
        ResponsePacket {
            room_id,
            content,
            sender_id: s
        }
    }

    /// Content From Vec
    /// 
    /// # Description
    /// Create a content from a vector of string
    /// 
    /// # Arguments
    /// * `c` Vec<String>
    /// 
    /// # Return
    /// String
    pub fn content_from_vec(c: Vec<String>) -> String {
        match serde_json::to_string(&c) {
            Ok(json) => json,
            Err(_) => String::new()
        }
    }
}

impl Stringifier for ResponsePacket {}

