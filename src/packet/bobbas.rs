use serde::{Serialize};
use super::Stringifier;
use crate::http::Bobbas;

#[derive(Serialize, Debug)]
pub struct BobbaList {
    pub bobbas: Vec<Bobbas>
}

impl Stringifier for BobbaList {}