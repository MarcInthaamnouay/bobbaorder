use ws::{listen, CloseCode, Handler, Handshake, Message, Result, Sender};
use crate::queue::producer;
use crate::core::logger::{log, Level};

pub mod sender;

// Message constants
const CLIENT_DONE_CON: &str = "The client is done with the connection.";
const CLIENT_AWAY_CON: &str = "The client is leaving the site.";

#[derive(Clone)]
struct Server {
    out: Sender
}

impl Handler for Server {
    fn on_open(&mut self, _: Handshake) -> Result<()> {
        producer::register_client(self.out.clone());

        Ok(())
    }

    fn on_message(&mut self, msg: Message) -> Result<()> {
        producer::push(msg, self.out.clone());
        Ok(())
    }

    fn on_close(&mut self, code: CloseCode, reason: &str) {
        match code {
            CloseCode::Normal => log(Level::Warning, CLIENT_DONE_CON),
            CloseCode::Away   => log(Level::Warning, CLIENT_AWAY_CON),
            _ => log(Level::Error, reason),
        }
    }
}

pub fn bootstrap_socket() {
    // room::create_default_room();
    println!("Socket server running on port 8088");

    listen("127.0.0.1:8088", |out| {
        Server {
            out
        }
    }).unwrap();
}
