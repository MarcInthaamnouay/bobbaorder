use ws::{Sender, Message};
use crate::core::logger::{log, Level};
use crate::packet::response::ResponsePacket;
use crate::packet::errors::ErrorPacket;
use crate::packet::Stringifier;

/// Send
///
/// # Description
/// Decorate the send method of the library ws::Sender
///
/// # Arguments
/// * `s` ws::Sender
pub fn send(s: &Sender, m: Message) {
    match s.send(m) {
        Ok(_) => {},
        Err(_) => log(Level::Warning, "Unable to send a message")
    }
}

/// Send Response Payload
///
/// # Description
/// Decorator method use to send a payload object with the ResponsePacket as a message
///
/// # Arguments
/// * `s` &Sender
/// * `id` String
/// * `content` String
/// * `sender_id` String
pub fn send_response_payload(s: &Sender, id: &str, content: &str, sender_id: String) {
    let packet = ResponsePacket {
        room_id: String::from(id),
        content: String::from(content),
        sender_id
    };
    let msg_packet = packet.to_message();
    send(s, msg_packet);
}

/// Send Error Payload
///
/// # Description
/// Decorator method use to send a payload object with the ErrorPacket as a message
///
/// # Arguments
/// * `&s` Sender
/// * `req` String
/// * `value` String
pub fn send_error_payload(s: &Sender, req: &str, value: &str) {
    let packet = ErrorPacket {
        request: String::from(req),
        value: String::from(value)
    };
    let msg_packet = packet.to_message();
    send(s, msg_packet);
}
