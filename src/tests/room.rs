#[cfg(test)]
mod room {
    use std::error::Error;
    use crate::room::{handler};

    #[test]
    fn test_create_room() {
        let name = String::from("foobar");
        match handler::create_room(Some(&name)) {
            Ok(res) => assert_eq!(res, "foobar"),
            Err(err) => panic!(err)
        }
    }

    #[test]
    fn test_create_room_failure() {
        match handler::create_room(None) {
            Ok(_) => panic!("Expect to not have create a room w/o a name"),
            Err(err) => assert_eq!(err.description(), "Unable to create room due to missing name parameter")
        }
    }

    #[test]
    fn test_delete_room_foobar() {
        let name = String::from("foobar");
        match handler::delete_room(Some(&name)) {
            Ok(res) => assert_eq!((), res),
            Err(err) => panic!(err)
        };
    }

    #[test]
    fn test_delete_unknown_room() {
        let name = String::from("barfoo");
        match handler::delete_room(Some(&name)) {
            Ok(_) => panic!("Expect to have fail as the room does not exist"),
            Err(err) => assert_eq!(err.description(), "Room does not exist")
        };
    }

    #[test]
    fn test_list_rooms() {
        let empty: Vec<String> = Vec::new();
        match handler::list_rooms() {
            Ok(res) => assert_eq!(res, empty),
            Err(err) => panic!(err)
        };
    }
}
