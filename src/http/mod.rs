use reqwest;
use serde::{Deserialize, Serialize};
use serde::de::{DeserializeOwned};

/// Bobbas
/// 
/// # Description
/// Structure describing a Bobba
#[derive(Deserialize, Serialize, Debug)]
pub struct Bobbas {
    id: u64,
    pub name: String,
    flavor: String
}

/// Api Response
/// 
/// # Description
/// Structure describing the representation of an API response
#[derive(Deserialize, Debug)]
pub struct ApiResponse {
    pub status: u64,
    pub data: Option<Vec<Bobbas>>
}

/// Bobba Order
/// 
/// # Description
/// Structure describing an order of a Bubble tea to be send to the API
#[derive(Serialize, Debug)]
pub struct BobbaOrder {
    bobba_id: u64,
    client_id: String
}

impl BobbaOrder {
    /// New
    /// 
    /// # Description
    /// Create a new bobba order
    /// 
    /// # Arguments
    /// * `bobbas` Bobbas
    /// * `client_id` String
    /// 
    /// # Return
    /// BobbaOrder
    pub fn new(bobba: Bobbas, client_id: String) -> BobbaOrder {
        BobbaOrder {
            bobba_id: bobba.id,
            client_id
        }
    }
}


/// HttpClient
/// 
/// # Description
/// Struct use to build the httpclient
pub struct HttpClient;

impl HttpClient {
    /// Retrieve Response<T>
    /// 
    /// # Description
    /// Retrieve a response from an endpoint and return the populated Struct that could be Deserialize
    /// 
    /// # Arguments
    /// * `endpoint` &str
    /// 
    /// # Return
    /// Result<T, <Box::dyn::error::Errors>>
    pub fn retrieve_ressources<T>(self, endpoint: &str) -> Result<T, Box<dyn std::error::Error>> where T: DeserializeOwned {
        let res: T = reqwest::get(endpoint)?.json()?;
        
        Ok(res)
    }

    /// Make Order<T>
    /// 
    /// # Description
    /// Make an order toward the API by providing any kind of Stuct that can the Serialize
    /// 
    /// # Arguments
    /// * `endpoint` &str
    /// * `payload` T
    /// 
    /// # Return
    /// Result<T, <Box::dyn::error::Errors>>
    pub fn make_order<T>(self, endpoint: &str, payload: T) -> Result<(), Box<dyn std::error::Error>> where T: Serialize {
        let client = reqwest::Client::new();
        client.post(endpoint)
            .json(&payload)
            .send()?;

        Ok(())
    }
}
