use rand::Rng;
use super::loader;
use crate::core::logger::{log, Level};

// Error constants
const UNABLE_READ_DICT_ERROR: &str = "Unable to retrieve the dictionnary";

/// Sentences Trait
///
/// # Description
/// Trait use to implement the assemble method for structure
pub trait Sentences {
    fn assemble(&self) -> String;
}

pub struct Expressions {
    pub fst: FirstExpression,
    pub scd: SecondExpression
}

pub struct FirstExpression {
    pub accroche: String,
    pub insult: String,
    pub common: String,
    pub sexual_expression: String,
    pub body_part: String,
    pub family_member: String,
    pub pleasure_expression: String
}

pub struct SecondExpression {
    pub accroche: String,
    pub insult: String,
    pub fighting_action: String,
    pub time_expression: String,
    pub family_member: String,
    pub sexual_action: String
}

impl Sentences for FirstExpression {
    fn assemble(&self) -> String {
        format!(
            "{} {} {} {} {} {} {}",
            &self.accroche,
            &self.insult,
            &self.common,
            &self.sexual_expression,
            &self.body_part,
            &self.family_member,
            &self.pleasure_expression
        )
    }
}

impl Sentences for SecondExpression {
    fn assemble(&self) -> String {
        format!(
            "{} {} {} {} {} {}",
            &self.accroche,
            &self.insult,
            &self.fighting_action,
            &self.time_expression,
            &self.family_member,
            &self.sexual_action
        )
    }
}

/// Retrieve Random Word From Vec
///
/// # Description
/// Retrieve a random string from a Vector
///
/// # Return
/// String
fn retrieve_random_word_from_vec(vec: Vec<String>) -> String {
    let mut rng = rand::thread_rng();
    let idx = rng.gen_range(0, vec.len() - 1);

    String::from(&vec[idx])
}

/// Generate Sentences
///
/// # Description
/// Factory which generate a sentence based on the dictionnary
///
/// # Return
/// Option<Expressions>
pub fn generate_sentences(words: String) -> Option<Expressions> {
    let dict_opt = loader::retrieve_swear_expressions_dictionnary(words);
    if dict_opt.is_none() {
        log(Level::Warning, UNABLE_READ_DICT_ERROR);
        return None;
    }

    // generate random number to choose a genre
    // 0 => male
    // 1 => female
    let mut rng = rand::thread_rng();
    let gender = rng.gen::<bool>();

    let dict = dict_opt.unwrap();

    // retrieve family member above in order to not move the value
    // dirty
    let first_member = dict.family.clone().retrieve_vec_from_gender(gender);
    let second_member = dict.family.retrieve_vec_from_gender(gender);
    // Generate the first expression
    let first_sentence = FirstExpression {
        accroche: retrieve_random_word_from_vec(dict.accroche),
        insult: retrieve_random_word_from_vec(dict.insult.accroche),
        common: retrieve_random_word_from_vec(dict.expressions.common),
        sexual_expression: retrieve_random_word_from_vec(dict.insult.sexual),
        body_part: retrieve_random_word_from_vec(dict.body_parts.retrieve_vec_from_gender(gender)),
        family_member: retrieve_random_word_from_vec(first_member),
        pleasure_expression: retrieve_random_word_from_vec(dict.expressions.sexual.retrieve_vec_from_gender(gender))
    };

    let second_sentence = SecondExpression {
        accroche: retrieve_random_word_from_vec(dict.expressions.actions),
        insult: retrieve_random_word_from_vec(dict.insult.uncategorized),
        fighting_action: retrieve_random_word_from_vec(dict.actions),
        time_expression: retrieve_random_word_from_vec(dict.expressions.duration),
        family_member: retrieve_random_word_from_vec(second_member),
        sexual_action: retrieve_random_word_from_vec(dict.expressions.nsfw_action)
    };

    Some(
        Expressions {
            fst: first_sentence,
            scd: second_sentence
        }
    )
}
