use std::fs;
use std::error::Error;
use serde::{Deserialize};
use serde_json;
use crate::core::logger::{log, Level};

// Constants
const STATIC_FILE_PATH: &str = "./static/dictionnary.json";

#[derive(Deserialize)]
pub struct SwearDictionnary {
    pub accroche: Vec<String>,
    pub insult: InsultExpressions,
    pub body_parts: FamilyExpressions,
    pub expressions: UsualExpression,
    pub family: FamilyExpressions,
    pub actions: Vec<String>
}

#[derive(Deserialize)]
pub struct InsultExpressions {
    pub accroche: Vec<String>,
    pub uncategorized: Vec<String>,
    pub sexual: Vec<String>,
    pub expression: Vec<String>
}

#[derive(Deserialize, Clone)]
pub struct FamilyExpressions {
    pub female: Vec<String>,
    pub male: Vec<String>
}

#[derive(Deserialize)]
pub struct UsualExpression {
    pub common: Vec<String>,
    pub sexual: FamilyExpressions,
    pub nsfw_action: Vec<String>,
    pub actions: Vec<String>,
    pub duration: Vec<String>
}

impl FamilyExpressions {
    /// Retrieve Vec From Gender
    ///
    /// # Description
    /// Retrieve a vector gender from
    ///
    /// # Arguments
    /// * `self` FamilyExpressions
    /// * `gender` bool
    ///
    /// # Return
    /// Vec<String>
    pub fn retrieve_vec_from_gender(self, gender: bool) -> Vec<String> {
        if gender {
            return self.female;
        }

        self.male
    }
}

/// Load Static Dictionnary
///
/// # Description
/// Load the static file representing the swear words
pub fn load_static_dictionnary() -> Option<String> {
    match fs::read_to_string(STATIC_FILE_PATH) {
        Ok(res) => Some(res),
        Err(err) => {
            log(Level::Error, err.description());
            None
        }
    }
}

/// Retrieve Swear Expression Dictionnary
///
/// # Description
/// Retrieve the dictionnary as a swear expression datastructure
///
/// # Return
/// Option<SwearDictionnary>
pub fn retrieve_swear_expressions_dictionnary(content: String) -> Option<SwearDictionnary> {
    let entity: SwearDictionnary = match serde_json::from_str(&content.as_str()) {
        Ok(res) => res,
        Err(err) => {
            log(Level::Error, err.description());
            return None;
        }
    };

    Some(entity)
}
