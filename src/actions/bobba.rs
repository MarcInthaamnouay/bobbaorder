use ws::Sender;
use super::visit::Visitor;
use crate::core::cmd::{Actions, get_action};
use crate::queue::QueuePacket;
use crate::http;
use crate::core::logger::{log, Level};
use crate::socket::sender;
use crate::packet::bobbas::BobbaList;
use crate::packet::Stringifier;

// Constant
const BOBBA_TARGET: &str = "bobbaChief";

// Endpoints
const RETRIEVE_BOBBA_ENDPOINT: &str = "http://127.0.0.1:9000/bobba";
const SEND_ORDER_ENDPOINT: &str = "http://127.0.0.1:9000/bobba/order";

// Error constant
const ORDER_SAVE_ERROR: &str = "Unable to save the order";
const BOBBA_NOT_FOUND_ERROR: &str = "Bobba not found in our inventory";

/// Bobba Action
///
/// # Description
/// Struct use to handle the bobba action
pub struct BobbaAction<'a> {
    pub packet: &'a QueuePacket,
}

impl<'a> Visitor<BobbaAction<'_>> for BobbaAction<'a> {
    fn visit(&self) {
        let sender_id = String::from(&self.packet.msg.sender_id);
        let (action, args) = get_action(&self.packet.msg.cmd.args);

        match action {
            Actions::Order => send_bobba_order_handler(args, sender_id, &self.packet.s),
            Actions::List => list_bobba_handler(args, sender_id, &self.packet.s),
            _ => {}
        }
    }
}

/// Get Bobbas From Resources
///
/// # Description
/// Retrieve bubble teas from an endpoint
///
/// # Return
/// Option<Vec<http::Bobbas>>
fn get_bobbas_from_resources() -> Option<Vec<http::Bobbas>> {
    let req = http::HttpClient {};

    let resources = req.retrieve_ressources::<http::ApiResponse>(RETRIEVE_BOBBA_ENDPOINT);
    if let Err(err) = resources {
        log(Level::Warning, err.description());
        return None;
    }

    let resp = resources.unwrap();
    resp.data.as_ref()?;

    Some(resp.data.unwrap())
}

/// Get Bobba By Name
///
/// # Description
/// Action which retrieve a bobba by an id from the list of bobba
///
/// # Arguments
/// * `name` Option<&String>
/// * `bobbas` Option<Vec<http::Bobbas>>
///
/// # Return
/// Option<http::Bobbas>
fn get_bobba_by_name(name: Option<&String>, bobbas: Option<Vec<http::Bobbas>>) -> Option<http::Bobbas> {
    if let Some(n) = name {
        if let Some(bb) = bobbas {
            let bobba: Option<http::Bobbas> = bb
                .into_iter()
                .filter(|b| &b.name == n)
                .last();

            return bobba;
        }
    }

    None
}

/// Send Bobba Order Handler
///
/// # Description
/// Retrieve a bobba and make an order to the API and send back a response to the user
///
/// # Arguments
/// * `args` Vec<String>
/// * `sender_id` String
/// * `s` &Sender
fn send_bobba_order_handler(args: Vec<String>, sender_id: String, s: &Sender) {
    let req = http::HttpClient {};
    let bobbas = get_bobbas_from_resources();
    let bobba = get_bobba_by_name(args.get(0), bobbas);

    if bobba.is_none() {
        sender::send_error_payload(s, "", BOBBA_NOT_FOUND_ERROR);
        return;
    }

    let payload = http::BobbaOrder::new(bobba.unwrap(), sender_id.clone());
    match req.make_order::<http::BobbaOrder>(SEND_ORDER_ENDPOINT, payload) {
        Ok(_) => sender::send_response_payload(s, BOBBA_TARGET, "Your bobba order has been made", sender_id),
        Err(err) => sender::send_error_payload(s, ORDER_SAVE_ERROR, err.description())
    };
}

/// List Bobba Handler
///
/// # Description
/// Get the list of bobbas and sent it back to the user
///
/// # Arguments
/// * `_` Vec<String>
/// * `sender_id` String
/// * `s` &Sender
fn list_bobba_handler(_: Vec<String>, sender_id: String, s: &Sender) {
    let bobba = get_bobbas_from_resources();

    if bobba.is_none() {
        sender::send_error_payload(s, "", BOBBA_NOT_FOUND_ERROR);
        return;
    }

    let stringify_bobbas = BobbaList {
        bobbas: bobba.unwrap()
    }.to_string();
    sender::send_response_payload(s, BOBBA_TARGET, stringify_bobbas.as_str(), sender_id);
}
