/// Room Handler
///
/// # Description
/// Handle the room action from a websocket macro level
use std::error::Error;
use ws::Sender;
use super::visit::{Visitor};
use crate::core::cmd::{Actions, get_action};
use crate::packet::response::{ResponsePacket};
use crate::packet::Stringifier;
use crate::room::handler;
use crate::socket::sender;
use crate::queue::QueuePacket;
use crate::room::{broadcast_default_room};
use crate::err::room::action::{
    CREATE_ROOM_ERROR,
    DELETE_ROOM_ERROR,
    JOIN_ROOM_ERROR,
    LIST_ROOM_ERROR
};

// Constant
const PRIVATE_TARGET: &str = "private";
const SOCKET_SENDER_ID: &str = "socket";

/// Room Action
///
/// # Description
/// Structure use for handling action made in a room
pub struct RoomAction<'a> {
    pub packet: &'a QueuePacket
}

impl<'a> Visitor<RoomAction <'_>> for RoomAction<'a> {
    fn visit(&self) {
        let sender_id = &self.packet.msg.sender_id;
        let (action, args) = get_action(&self.packet.msg.cmd.args);

        match action {
            Actions::Create => create_room_handler(args.get(0), &self.packet.s, sender_id),
            Actions::Join => join_room_handler(args.get(0), &self.packet.s),
            Actions::List => list_room_handler(&self.packet.s),
            Actions::Delete => delete_room_handler(args.get(0), &self.packet.s),
            _ => ()
        }
    }
}

/// Create Room Handler
///
/// # Description
/// Handle creating room action
///
/// # Arguments
/// * `n` Option<&String>
/// * `s` Sender
/// * `sender_id` &str
fn create_room_handler(n: Option<&String>, s: &Sender, sender_id: &str) {
    match handler::create_room(n) {
        Ok(r) => sender::send_response_payload(s, r.as_str(), "successfully creating room", sender_id.to_owned()),
        Err(err) => sender::send_error_payload(s, CREATE_ROOM_ERROR, err.description())
    }
}

/// Delete Room Handler
///
/// # Description
/// Handle the delete room action
///
/// # Arguments
/// * `n` Option<&String>
fn delete_room_handler(n: Option<&String>, s: &Sender) {
    match handler::delete_room(n) {
        Ok(_) => {
            let packet = ResponsePacket::new(
                "default".to_string(),
                format!("The room {:?} has been deleted", n.unwrap()),
                "all".to_string()
            ).to_message();
            broadcast_default_room(packet);
        },
        Err(err) => sender::send_error_payload(s, DELETE_ROOM_ERROR, err.description())
    }
}

/// Join Room Handler
///
/// # Description
/// Handle the join room action
///
/// # Arguments
/// * `n` Option<&String>
/// * `s` Sender
fn join_room_handler(n: Option<&String>, s: &Sender) {
    match handler::join_room(n, &s) {
        Ok(_) => {},
        Err(err) => sender::send_error_payload(s, JOIN_ROOM_ERROR, err.description())
    }
}

/// List Room Handler
///
/// # Description
/// Handle the list room action
///
/// # Arguments
/// * `s` &Sender
fn list_room_handler(s: &Sender) {
    match handler::list_rooms() {
        Ok(r) => {
            let content = ResponsePacket::content_from_vec(r);
            sender::send_response_payload(s, PRIVATE_TARGET, content.as_str(), SOCKET_SENDER_ID.to_string());
        },
        Err(err) => sender::send_error_payload(s, LIST_ROOM_ERROR, err.description())
    }
}
