pub mod room;
pub mod insult;
pub mod send;
pub mod bobba;

pub mod visit {
    /// Visitor
    /// 
    /// # Description
    /// Trait use to implement the visitor pattern
    pub trait Visitor<T> {
        /// Visit
        /// 
        /// # Description
        /// Visit the targeted datastructure
        fn visit(&self);
    }
}