/// Cursing module
///
/// # Description
/// This module generate random insult based on the Insultron insult generator. This has been requested for a workshop and made for fun as a challenge on how to construct a sentence. This module DOES NOT REFLECT MY MINDSET, MY OPINION AND MY THINKING. List of french insult has been retrieved here: http://cruciverbiste.club/index.php?id_cms=131&controller=cms
///
/// # Structure of a french insult (based on what people from Paris and it's suburb say)
/// An insult has the following structure
/// <Accroche> |  <insult> | <common expression> | <sexual expression> | <sex body part> | <person>  | <pleasure expression> |. <Accroche second subject> | <insult> | <fighting action> | <time expression> | <family> | <sexual action>
///
/// # Example
/// Espèce de | beauf | hier soir j'ai | niqué | le <sexual part> | de ta go | elle a tellement kiffé qu'elle m'a <action> jusqu'à <something> | Je ne sais pas ce qui me retiens de | t'éclater ... | pendant que | ta go | se <action> en pensant à moi
///
/// After generating these insults better listen to some Kpop drama OST songs
use super::visit::Visitor;
use crate::swear::sentence::{generate_sentences, Sentences};
use crate::packet::message::MessagePacket;
use crate::packet::Stringifier;
use crate::core::cmd::Command;
use crate::queue;

/// Insult Action
///
/// # Description
/// Struct use to handle the insult action
pub struct InsultAction<'a> {
    pub packet: &'a queue::QueuePacket,
    pub words: &'a Option<String>
}

impl<'a> Visitor<InsultAction<'_>> for InsultAction<'a> {
    fn visit(&self) {
        let msg = create_message_packet(&self.packet.msg, &self.words);
        let socket = &self.packet.s;
        let packet = msg.to_message();

        // Create a new task that will be push which will be consume as a regular message to be sent
        queue::producer::push(packet, socket.clone());
    }
}

/// Generate Swear Sentence
///
/// # Description
/// (Decorator / Adapter  ? no adaptee thought)  which wrap the generation of the swear sentences
///
/// # Return
/// String
fn generate_swear_sentence(words: &Option<String>) -> String {
    if let Some(word) = words {
        let sentences = generate_sentences(String::from(word));
        if sentences.is_none() {
            return String::new();
        }

        let expressions = sentences.unwrap();
        let first_expression = expressions.fst.assemble();
        let second_expression = expressions.scd.assemble();

        return format!("{}.{}", first_expression, second_expression)
    }

    String::new()
}

/// Create Message Packet
///
/// # Description
/// Create a new message packet with the insult as the content
///
/// # Arguments
/// * `packet` MessagePacket
///
/// # Return
/// MessagePacket
pub fn create_message_packet(packet: &MessagePacket, words: &Option<String>) -> MessagePacket {
    let mut msg = packet.clone();
    msg.content = generate_swear_sentence(words);
    msg.cmd = Command {
        name: None,
        args: None
    };

    msg
}
