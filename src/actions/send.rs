/// Send Handler
/// 
/// # Description
/// Handler use to transit packets
use std::error::Error;
use ws::Sender;
use super::visit::Visitor;
use crate::socket::sender;
use crate::queue::QueuePacket;
use crate::room::handler;
use crate::packet::message::{MessagePacket};
use crate::err::room::action::{SEND_ERROR};

/// Send Action
/// 
/// # Description
/// Struct use to handle to transit the regular packet action
pub struct SendAction<'a> {
    pub packet: &'a QueuePacket
}

impl<'a> Visitor<SendAction<'_>> for SendAction<'a> {
    fn visit(&self) {
        let packet = &self.packet.msg;
        broadcast_handler(packet, &self.packet.s);
    }
}

/// Broadcast Handler
/// 
/// # Description
/// Decorator which broadcast a message to a room handler
/// 
/// # Arguments
/// * `msg` MessagePacket
/// * `s` Sender
fn broadcast_handler(msg: &MessagePacket, s: &Sender) {
    match handler::broadcast_message(msg.clone()) {
        Ok(()) => {},
        Err(err) => sender::send_error_payload(s, SEND_ERROR, err.description())
    }
}
