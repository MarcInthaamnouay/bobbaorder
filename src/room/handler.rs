/// Handler module
///
/// # Description
/// Handle room action at a Queue macro level
use std::cell::RefCell;
use std::error::Error;
use ws::Sender;
use crate::core::logger::{Level, log};
use crate::core::errors::room::{
    MISSING_NAME_ERROR,
    ROOM_EXIST_ERROR,
    ROOM_NOT_EXIST_ERROR,
    USER_ALREADY_EXIST,
    NAME_EMPTY_ERROR,
    UNABLE_SEND_MESSAGE
};
use crate::room::{ROOMS, Room};
use crate::err::{BobbaErr, LOCK_ACQUIRE_CONSUMER_ERROR};
use crate::packet::message::{MessagePacket};
use crate::packet::response::{ResponsePacket};
use crate::packet::Stringifier;

/// Create Room
///
/// # Description
/// Create a room
///
/// # Arguments
/// * `name` Option<&String> ref to a string
///
/// # Return
/// Result<String, BobbaErr>
pub fn create_room(name: Option<&String>) -> Result<String, BobbaErr> {
    if name.is_none() {
        log(Level::Error, MISSING_NAME_ERROR);
        return Err(BobbaErr::new(MISSING_NAME_ERROR));
    }

    let n = name.unwrap();
    log(Level::Info, format!("Creating room with name {}", n).as_str());

    // Trying to acquire the lock on the Room resources
    let acquire_lock = ROOMS.lock();
    // If we can't acquire the lock return an error
    if let Err(err) = &acquire_lock {
        log(Level::Error, err.description());
        return Err(BobbaErr::new(LOCK_ACQUIRE_CONSUMER_ERROR));
    }

    // Retrieve the lock
    let mut lock = acquire_lock.unwrap();
    let room = lock.get_mut(n);
    if let Some(r) = room {
        log(Level::Error, format!("{}{}", ROOM_EXIST_ERROR, r.name).as_str());
        return Err(BobbaErr::new(ROOM_EXIST_ERROR));
    }

        // Otherwise insert a new room
    lock.insert(
        n.clone(),
        Room {
            name: String::from(n),
            senders: RefCell::new(Vec::new())
        }
    );

    Ok(String::from(n))
}

/// Join Room
///
/// # Description
/// Join an existing room
///
/// # Arguments
/// * `name` String
/// * `s` ws::Sender
pub fn join_room(name: Option<&String>, s: &Sender) -> Result<(), BobbaErr> {
    if name.is_none() {
        return Err(BobbaErr::new(NAME_EMPTY_ERROR));
    }

    // Trying to acquire the lock on the Room resources
    let acquire_lock = ROOMS.lock();
    if let Err(err) = &acquire_lock {
        log(Level::Error, err.description());
        return Err(BobbaErr::new(LOCK_ACQUIRE_CONSUMER_ERROR));
    }

    let mut lock = acquire_lock.unwrap();
    let room = lock.get_mut(name.unwrap());
    if room.is_none() {
        log(Level::Warning, ROOM_NOT_EXIST_ERROR);
        return Err(BobbaErr::new(ROOM_NOT_EXIST_ERROR));
    }

    let r = room.unwrap();
    let con_id = s.connection_id();

    // Check if the user already exist in the room
    let users = r.senders.get_mut();
    let mut user: Vec<&mut Sender> = users
        .iter_mut()
        .filter(|u| u.connection_id() == con_id)
        .collect();

    let user_opt = user.pop();
    if user_opt.is_some() {
        log(Level::Warning, USER_ALREADY_EXIST);
        return Err(BobbaErr::new(USER_ALREADY_EXIST));
    }

    // Otherwise push the new user
    users.push(s.clone());

    // Create a welcoming package
    let packet = ResponsePacket::new(
        String::from(name.unwrap()),
        format!("Welcome ! {} you've just join the room: {}", con_id, name.unwrap()),
        "all".to_string()
    ).to_message();
    // Send the packet to the entire room
    r.broadcast_room(packet);

    Ok(())
}

/// Delete Room
///
/// # Description
/// Delete a room
///
/// # Arguments
/// * `name`: String
pub fn delete_room(name: Option<&String>) -> Result<(), BobbaErr> {
    if name.is_none() {
        return Err(BobbaErr::new(NAME_EMPTY_ERROR));
    }

    let n = name.unwrap();
    let acquire_lock = ROOMS.lock();
    if let Err(err) = &acquire_lock {
        log(Level::Warning, err.description());
        return Err(BobbaErr::new(LOCK_ACQUIRE_CONSUMER_ERROR));
    }

    let mut lock = acquire_lock.unwrap();
    let item_opt = lock.remove(n);
    if item_opt.is_none() {
        return Err(BobbaErr::new(ROOM_NOT_EXIST_ERROR));
    }

    Ok(())
}

/// List Rooms
///
/// # Description
/// List the rooms available in the server
///
/// # Return
/// Result<Vec<String>>
pub fn list_rooms() -> Result<Vec<String>, BobbaErr> {
    let acquire_lock = ROOMS.lock();
    if let Err(err) = &acquire_lock {
        log(Level::Warning, err.description());
        return Err(BobbaErr::new(LOCK_ACQUIRE_CONSUMER_ERROR));
    }

    let lock = acquire_lock.unwrap();
    let rooms: Vec<String> = lock
        .iter()
        .map(|(key, _)| String::from(key))
        .collect();

    Ok(rooms)
}

/// Broadcast Message
///
/// # Description
/// Broadcast a message to the room
///
/// # Arguments
/// * `msg` MessagePacket
/// * `retry` u8
///
/// # Return
/// Result<(), ()>
pub fn broadcast_message(msg: MessagePacket) -> Result<(), BobbaErr> {
    let lock = ROOMS.lock();
    if lock.is_err() {
        log(Level::Error, UNABLE_SEND_MESSAGE);
        return Err(BobbaErr::new(UNABLE_SEND_MESSAGE));
    }

   let mut rooms = lock.unwrap();
   let room_opt = rooms.get_mut(&msg.room_id);
   if let Some(r) = room_opt {
       let packet = msg.create_from_message_packet();
       r.broadcast_room(packet.to_message());
   }

    Ok(())
}
