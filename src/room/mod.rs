use std::sync::Mutex;
use std::cell::RefCell;
use std::collections::HashMap;
use std::error::Error;
use ws::{Sender, Message};
use crate::core::logger::{Level, log};
use crate::core::errors::room::{LOCK_ALREADY_ACQUIRE_ERROR};

pub mod handler;

#[derive(Debug)]
pub struct Room {
    pub name: String,
    pub senders: RefCell<Vec<Sender>>
}

lazy_static! {
    #[derive(Debug)]
    pub static ref ROOMS: Mutex<HashMap<String, Room>> = Mutex::new(HashMap::new());
}

impl Room {
    /// Broadcast Room
    ///
    /// # Description
    /// Broadcast a message to a room
    ///
    /// # Arguments
    /// * `self` &Room
    /// * `msg` Message
    pub fn broadcast_room(&mut self, msg: Message) {
        let senders: &Vec<Sender> = self.senders.get_mut();
        for s in senders {
            if let Err(e) = s.send(msg.clone()) {
                log(Level::Warning, e.description());
            }
        }
    }
}

/// Broadcast Default Room
///
/// # Description
/// Broadcast message to a default room
///
/// # Arguments
/// * `msg` Message
pub fn broadcast_default_room(msg: Message) {
    let lock = ROOMS.lock();
    if lock.is_err() {
        log(Level::Error, LOCK_ALREADY_ACQUIRE_ERROR);
        return;
    }

    if let Ok(mut guard) = lock {
        let room = guard.get_mut(&"default".to_string());
        if let Some(r) = room {
            r.broadcast_room(msg);
        }
    }
}

/// Create Default Room
///
/// # Description
/// Create a default room
pub fn create_default_room() -> Result<(), &'static str> {
    let lock = ROOMS.lock();
    if lock.is_err() {
        log(Level::Error, LOCK_ALREADY_ACQUIRE_ERROR);
        return Err(LOCK_ALREADY_ACQUIRE_ERROR);
    }

    if let Ok(mut rooms) = lock {
        rooms.insert(
            "default".to_string(),
            Room {
                name: "default".to_string(),
                senders: RefCell::new(Vec::new())
            }
        );
    }

    Ok(())
}
