pub mod room;

use std::error::Error;
use std::fmt;

// Error constant
pub const LOCK_ACQUIRE_CONSUMER_ERROR: &str = "Lock has already been acquire by the consumer";
pub const LOCK_ACQUIRE_DIFFERENT_THREAD_ERROR: &str = "Lock has already been acquire by the producer";

/// Bobba Err
/// 
/// # Description
/// Custom error handler
#[derive(Debug)]
pub struct BobbaErr {
    reason: String
}

impl BobbaErr {
    /// New
    /// 
    /// # Arguments
    /// * `msg` &str
    /// 
    /// # Return
    /// BobbaErr
    pub fn new(msg: &str) -> BobbaErr {
        BobbaErr {
            reason: String::from(msg)
        }
    }
}

impl fmt::Display for BobbaErr {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f,"{}", self.reason)
    }
}

impl Error for BobbaErr {
    fn description(&self) -> &str {
        &self.reason
    }
}
