/// Action
/// 
/// # Description
/// Module use to register the error related to the room
pub mod action {
    /// Error list
    pub const CREATE_ROOM_ERROR: &str = "Error while creating a room ";
    pub const DELETE_ROOM_ERROR: &str = "Unable to delete the targeted room of name: ";
    pub const JOIN_ROOM_ERROR: &str = "Unable to join room of name: ";
    pub const LIST_ROOM_ERROR: &str = "Unable to list the rooms";
    pub const SEND_ERROR: &str = "Unable to send the message";
}